import Vue from 'vue'
import './plugins/vuetify'
import App from './App.vue'
import router from './router'
import './registerServiceWorker'
// import 'roboto-fontface/css/roboto/roboto-fontface.css'
import './fonts/vazir.css'
import 'material-design-icons-iconfont/dist/material-design-icons.css'

Vue.config.productionTip = false

new Vue({
  router,
  data: () => ({
    currentScene: {},
    snackbar: {
      show: false,
      text: ''
    },
    baseUrl: process.env.BASE_URL
  }),
  methods: {
    alert (message) {
      this.snackbar.show = true
      this.snackbar.text = message
    },
    httpCatcher (err) {
      console.error(err)
      this.$root.snackbar.show = true
      try {
        this.$root.snackbar.text = err.response.data.message
      } catch (e) {
        this.$root.snackbar.text = err.message
      }
    }
  },
  render: h => h(App)
}).$mount('#app')

setTimeout(() => {

}, 5000)

document.addEventListener('deviceready', function () {
  // cordova test
  // window.location.href = 'http://192.168.43.229:8080'
}, false)
