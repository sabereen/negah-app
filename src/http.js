import axios from 'axios'
import Vue from 'vue'

const http = axios.create({
  baseURL: Vue.config.devtools ? `${location.protocol}//${location.hostname}:6672/api` : 'http://negah.si2.ir/api'
  // baseURL: 'http://negah.si2.ir/api',
})

http.interceptors.request.use(config => {
  config.headers['Authorization'] = 'Bearer ' + window.localStorage.getItem('token')
  return config
}, error => {
  return Promise.reject(error)
})

// http.interceptors.response.use(function (response) {
//   // Do something with response data
//   return response
// }, function (error) {
//   // Do something with response error
//   return Promise.reject(error)
// })

// هر ۲۵ دقیقه توکن جدید بگیرد
var interval = setInterval(() => {
  var user = window.localStorage.getItem('user')
  if (!user) {
    window.clearInterval(interval)
    return
  }

  refreshToken()
}, 29 * 60 * 1000)

export function refreshToken (interval) {
  let user = window.localStorage.getItem('user')
  if (!user) {
    if (interval) {
      window.clearInterval(interval)
    }
    return false
  }
  user = JSON.parse(user)

  http.post('auth/refresh', {
    userId: user._id,
    refreshToken: localStorage.getItem('refreshToken')
  })
    .then(result => {
      window.localStorage.setItem('token', result.data.token)
      window.localStorage.setItem('user', JSON.stringify(result.data.userInfo))
    })
    .catch(err => {
      console.log(err)
    })

  return true
}

refreshToken()

export default http
