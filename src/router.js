import Vue from 'vue'
import Router from 'vue-router'
import Login from './views/Login.vue'
// import Home from './views/Home.vue'
import Scenes from './views/Scenes.vue'
import Scene from './views/Scene.vue'
import Tickets from './views/Tickets.vue'

Vue.use(Router)

const router = new Router({
  routes: [
    {
      path: '',
      name: 'home',
      component: Scenes
    },
    {
      path: '/login',
      name: 'login',
      component: Login
    },
    {
      path: '/scenes',
      name: 'scenes',
      component: Scenes
    },
    {
      path: '/scene/:sceneId',
      name: 'scene',
      component: Scene
    },
    {
      path: '/tickets/:sceneId',
      name: 'tickets',
      component: Tickets
    }
    // {
    //   path: '/about',
    //   name: 'about',
    //   component: () => import(/* webpackChunkName: "about" */ './views/About.vue')
    // }
  ]
})

// require scene guard
router.beforeEach((to, from, next) => {
  if (to.name === 'scene' || to.name === 'tickets') {
    if (/^[\dabcdef]{24}$/.test(to.params.sceneId)) {
      next()
    } else {
      next('/scenes')
    }
  } else {
    next()
  }
})

// Login Guard
router.beforeEach((to, from, next) => {
  if (to.name !== 'login') {
    let token = window.localStorage.getItem('token')
    if (token) {
      next()
    } else {
      next({ name: 'login', path: '/login' })
    }
  } else {
    next()
  }
})

export default router
