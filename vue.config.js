module.exports = {
  lintOnSave: false,
  publicPath: './',

  pwa: {
    name: 'نگاه'
  },

  devServer: {
    watchOptions: {
      poll: true,
      disableHostCheck: true
    }
  }
}
